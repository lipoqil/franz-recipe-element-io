// Zalo integration
const path = require('path');

module.exports = (Franz, options) => {
  function getMessages() {
    var count = Array.from(
        document
          .querySelectorAll('//*[@id="matrixchat"]/div[1]/div[2]/div[1]/aside/div[3]/div/div/div[2]/div[1]/div/div[3]/div/span.mx_NotificationBadge_count')
      )
      .reduce((total, current) => total += parseInt(current.innerText), 0);
    Franz.setBadge(count);
  };

  function init() {
    setTimeout(function(){
      window.location.href = 'https://app.element.io/#/home'
    }, 1000);
  };

  Franz.loop(getMessages);
  Franz.initialize(init);
};
